﻿using eslib.nnp5;
using System;

namespace testPkgDefine
{
    public class TestPkg : IPackage
    {
        public byte[] buf;

        public TestPkg()
        {
        }

        public TestPkg(byte[] buf)
        {
            this.buf = buf;
        }

        public byte[] GetBuffer()
        {
            return buf;
        }


        public override string ToString()
        {
            string ret = "";
            foreach (byte b in buf)
            {
                ret += $"{b:X} ";
            }
            return ret;
        }
    }
}
