﻿using eslib.nnp5;

namespace microFFTestClient
{
    class Manager : ConnectorManager
    {
        public Manager() : base(true)
        {
        }

        public override IAppBase CreateAppUser(Connector connector)
        {
            return new App(connector);
        }
    }
}
