﻿using eslib.nnp5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microFFTestClient
{
    class Program
    {
        Manager mng;
        TCPConnector conn;

        public Program()
        {
            Task.Run(async () =>
            {
                await Task.Delay(500);

                mng = new Manager();
                conn = new TCPConnector("127.0.0.1", 62000);
                mng.PushConnector(conn);
            });
        }

        static void Main(string[] args)
        {
            new Program();
            Console.ReadKey(true);
        }
    }
}
