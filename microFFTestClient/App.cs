﻿using System;
using System.Threading;
using System.Threading.Tasks;
using eslib.nnp5;
using testPkgDefine;

namespace microFFTestClient
{
    class App : PkgHandlerApp
    {
        public App(Connector connector) : base(connector)
        {
            Console.WriteLine("已连接 ");

            //发送测试
            Task.Run(() =>
            {
                TestPkg pkg = new TestPkg(new byte[] { 0x43, 0xff, 0x44, 0xff });
                SendObjectPkg(pkg);
            });
        }



        [PkgHandle]
        public void Handle(TestPkg p)
        {
            Console.WriteLine($"TestPkg：{p}");
        }

        public override void DefaultHandle(object p)
        {
            Console.WriteLine($"默认数据包处理：{p}");
        }


        public override void DisconnectEvent(Connector connector, string message)
        {
            Console.WriteLine(message);
        }

    }
}
