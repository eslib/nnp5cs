﻿using eslib.nnp5;
using eslib.nnp5.serializer;

namespace test
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BsonSerializer ser = new();

            Test t1 = new(134, "test");
            byte[] buf = ser.Serialize(t1);
            Test t2 = ser.Deserialize<Test>(buf);
        }
    }

}
