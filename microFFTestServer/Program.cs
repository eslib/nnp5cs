﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using eslib.nnp5;

namespace microFFTestServer
{
    class Program
    {
        Manager mng;
        TCPServerListener listener;

        public Program()
        {
            int port = 62000;

            mng = new Manager();
            listener = new TCPServerListener();
            listener.bind(port);
            mng.PushServerListener(listener);
            mng.OnlineAppsCountChangeEvent += Mng_OnlineAppsCountChangeEvent;

            Console.WriteLine($"监听端口{port}..");
        }

        private void Mng_OnlineAppsCountChangeEvent(int count)
        {
            Console.WriteLine($"连接数:{count}");
        }

        static void Main(string[] args)
        {
            new Program();
            Console.ReadKey(true);
        }
    }
}
