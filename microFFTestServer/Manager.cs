﻿using eslib.nnp5;

namespace microFFTestServer
{
    class Manager : ConnectorManager
    {
        public Manager() : base(false)
        {
        }

        public override IAppBase CreateAppUser(Connector connector)
        {
            return new App(connector);
        }
    }
}
