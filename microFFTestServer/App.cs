﻿using System;
using System.Threading;
using System.Threading.Tasks;
using eslib.nnp5;
using testPkgDefine;

namespace microFFTestServer
{
    class App : PkgHandlerApp
    {
        public App(Connector connector) : base(connector)
        {
            //发送测试
            Task.Run(() =>
            {
                Thread.Sleep(500);
                TestPkg pkg = new TestPkg(new byte[] { 0x41, 0xff, 0x42 });
                SendObjectPkg(pkg);
            });
        }


        [PkgHandle]
        public void Handle(TestPkg p)
        {
            Console.WriteLine($"TestPkg：{p}");
        }

        public override void DefaultHandle(object p)
        {
            Console.WriteLine($"默认数据包处理：{p}");
        }



        public override void DisconnectEvent(Connector connector, string message)
        {
            Console.WriteLine("连接断开");
        }

    }
}
