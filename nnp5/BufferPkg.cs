﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5
{

    /// <summary>
    /// 缓存数据包（最基本的数据包)
    /// </summary>
    public class BufferPkg : IPackage
    {
        /// <summary>
        /// 字节缓存
        /// </summary>
        public List<byte> buffer { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public BufferPkg()
        {
            buffer = new List<byte>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        public BufferPkg(byte[] buffer)
        {
            this.buffer = new List<byte>(buffer);
        }


        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <returns></returns>
        public byte[] GetBuffer()
        {
            return buffer.ToArray();
        }
    }
}
