﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5.layers
{
    /// <summary>
    /// 传输层异常
    /// </summary>
    public class TransportLayerException : Exception
    {
        public TransportLayerException() { }

        public TransportLayerException(string msg) : base(msg) { }
    }
}
