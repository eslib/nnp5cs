﻿namespace eslib.nnp5
{
    /// <summary>
    /// 发送器模式
    /// </summary>
    public enum SenderMode
    {
        /// <summary>
        /// 需要ack模式
        /// </summary>
        AckMode = 0,

        /// <summary>
        /// 不需要ack模式
        /// </summary>
        NoAckMode
    }

}
