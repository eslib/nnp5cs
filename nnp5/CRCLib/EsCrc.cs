﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DamienG.Security.Cryptography;


namespace eslib.CRClib
{
    public class EsCrc
    {
        #region Public Methods

        /// <summary>
        /// uint转字节
        /// </summary>
        /// <param name="crc"></param>
        /// <returns></returns>
        public static byte[] Uint2Bytes(UInt32 crc)
        {
            return BitConverter.GetBytes(crc);
        }


        /// <summary>
        /// Default Encoding as GB2312
        /// Default seed of 0xffffffff
        /// Default poly of 0xedb88320
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public UInt32 ComputeCrc(string str)
        {
            return ComputeCrc(str, "GB2312", 0xffffffff, 0xedb88320);
        }






        /// <summary>
        /// Default seed of 0xffffffff
        /// Default poly of 0xedb88320
        /// </summary>
        /// <param name="str">the input string</param>
        /// <param name="encoding">Encoding of the string</param>
        /// <returns></returns>
        public UInt32 ComputeCrc(string str, string encoding)
        {
            byte[] bytes = Encoding.GetEncoding(encoding).GetBytes(str);

            return Crc32.Compute(bytes);

        }




        /// <summary>
        /// Default seed of 0xffffffff
        /// Default poly of 0xedb88320
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public UInt32 ComputeCrc(byte[] bytes)
        {
            return Crc32.Compute(bytes);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="seed">Default is 0xffffffff</param>
        /// <param name="poly">Default is 0xedb88320</param>
        /// <returns></returns>
        public UInt32 ComputeCrc(byte[] bytes, UInt32 seed, UInt32 poly)
        {
            return Crc32.Compute(poly, seed, bytes);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="encoding"></param>
        /// <param name="seed">Default is 0xffffffff</param>
        /// <param name="poly">Default is 0xedb88320</param>
        /// <returns></returns>
        public UInt32 ComputeCrc(string str, string encoding, UInt32 seed, UInt32 poly)
        {
            byte[] bytes = Encoding.GetEncoding(encoding).GetBytes(str);

            return Crc32.Compute(poly, seed, bytes);
        }
        #endregion
    }
}
