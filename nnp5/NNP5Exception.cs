﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eslib.nnp5
{
    public class NNP5Exception : Exception
    {
        public NNP5Exception()
        {
        }

        public NNP5Exception(string message) : base(message)
        {
        }
    }
}
