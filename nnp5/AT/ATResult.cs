﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5.AT
{
    /// <summary>
    /// AT指令分析结果
    /// </summary>
    public class ATResult
    {
        /// <summary>
        /// 参数计数(包括AT命令)
        /// </summary>
        public int Count { get { return Params.Count; } }

        /// <summary>
        /// 参数集合(第一个是AT命令)
        /// </summary>
        public List<ATParam> Params { get; private set; } = new List<ATParam>();

        /// <summary>
        /// 获取命令参数
        /// </summary>
        /// <returns></returns>
        public string Command
        {
            get
            {
                if (Count < 1) throw new ATException("AT不正确");
                if (Params[0].ParamType != ATParmaType.Command) throw new NNP5Exception("AT不正确");
                return Params[0].Value;
            }
        }


        /// <summary>
        /// 获取普通参数集合(不包括命令参数）
        /// </summary>
        public ATParam[] NormalParams
        {
            get
            {
                List<ATParam> ret = new List<ATParam>();
                if (!IsSingleAT)
                {
                    for (int i = 1; i < Count; i++)
                    {
                        ret.Add(Params[i]);
                    }
                }
                return ret.ToArray();
            }
        }



        /// <summary>
        /// 是否为单命令型AT指令（无参数）
        /// </summary>
        public bool IsSingleAT
        {
            get
            {
                return Count == 1;
            }
        }



        /// <summary>
        /// 是否查询型AT指令
        /// </summary>
        public bool IsQuestionAT
        {
            get
            {
                if (Count > 1)
                {
                    return Params[1].ParamType == ATParmaType.Question;
                }
                return false;
            }
        }

        /// <summary>
        /// 参数索引（索引0为命令参数)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ATParam this[int index]
        {
            get
            {
                return Params[index];
            }
        }

        /// <summary>
        /// 转换为AT指令(包含AT+头和\r\n结尾)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "AT+";
            if (Count > 0) ret += Params[0].Value;

            if (Count > 1)
            {
                if (Params[1].ParamType == ATParmaType.Question)
                {
                    ret += "?";

                }
                else
                {
                    ret += "=";
                    for (int i = 1; i < Count; i++)
                    {
                        ret += Params[i].Value;
                        if (i != Count - 1) ret += ",";
                    }
                }
            }

            return ret + "\r\n";
        }


        public string ToATString()
        {
            return ToString();
        }

    }



    /// <summary>
    /// at参数
    /// </summary>
    public class ATParam
    {
        public ATParam(ATParmaType type, string value)
        {
            this.ParamType = type;
            this.Value = value;
        }

        /// <summary>
        /// 参数类型
        /// </summary>
        public ATParmaType ParamType { get; internal set; } = ATParmaType.Normal;

        /// <summary>
        /// 获取字符串值
        /// </summary>
        public string Value { get; private set; } = "";

        /// <summary>
        /// 获取整数值
        /// </summary>
        public int IntValue
        {
            get
            {
                return int.Parse(Value);
            }
        }

        /// <summary>
        /// 获取浮点数值
        /// </summary>
        public double DoubleValue
        {
            get
            {
                return double.Parse(Value);
            }
        }
    }


    /// <summary>
    /// at参数类型
    /// </summary>
    public enum ATParmaType
    {
        /// <summary>
        /// 命令参数
        /// </summary>
        Command,

        /// <summary>
        /// 代表问号
        /// </summary>
        Question,

        /// <summary>
        /// 普通参数
        /// </summary>
        Normal
    }
}
