﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5.AT
{
    /// <summary>
    /// AT分析器异常
    /// </summary>
    public class ATException : Exception
    {
        public ATException(string message) : base(message)
        {
        }
    }
}
