﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5
{
    /// <summary>
    /// 通讯层异常
    /// </summary>
    public class CommunicaException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public CommunicaException(string message) : base(message) { }
    }
}
