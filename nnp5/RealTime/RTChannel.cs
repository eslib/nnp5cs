﻿namespace eslib.nnp5.RealTime
{
    /// <summary>
    /// 实时通道(线程同步）
    /// </summary>
    internal sealed class RTChannel
    {
        /// <summary>
        ///  [线程同步的]设置/获取数据包
        ///  获取成功后返回并自动移除,为空返回null
        /// </summary>
        public IPackage Package
        {
            get
            {
                lock (this)
                {
                    IPackage res = pkg;
                    pkg = null;
                    return res;
                }
            }
            set
            {
                lock (this)
                {
                    pkg = value;
                }
            }
        }
        IPackage pkg = null;

    }
}
