﻿using System;

namespace eslib.nnp5
{
    /// <summary>
    /// 数据包抽象
    /// </summary>
    public interface IPackage
    {
        /// <summary>
        /// 获取包数据 
        /// </summary>
        /// <returns></returns>
        byte[] GetBuffer();
        
    }
}
