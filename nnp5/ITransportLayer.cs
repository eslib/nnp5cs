﻿namespace eslib.nnp5.layers
{
    /// <summary>
    /// 传输层接口
    /// </summary>
    public interface ITransportLayer
    {
        /// <summary>
        /// 连接器
        /// </summary>
        Connector connector { get; }

        /// <summary>
        /// 新数据包事件,具体数据包由派生类决定
        /// </summary>
        event NewTransportLayerDataEventHandler NewTransportLayerDataEvent;

        /// <summary>
        /// 重置
        /// </summary>
        void Reset();

        /// <summary>
        /// 发送数据,data为原始数据
        /// </summary>
        /// <param name="data">原始数据</param>
        void sendData(byte[] data);
    }



    /// <summary>
    /// 传输层数据包接收事件委托
    /// </summary>
    /// <param name="buffer"></param>
    public delegate void NewTransportLayerDataEventHandler(byte[] buffer);

}