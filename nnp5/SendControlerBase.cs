﻿namespace eslib.nnp5.layers
{


    /// <summary>
    /// 数据包发送器
    /// </summary>
    public abstract class SendControlerBase
    {
        /// <summary>
        /// 连接器
        /// </summary>
        protected Connector connector;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connector">通讯层连接对象</param>              
        public SendControlerBase(Connector connector)
        {
            ResetQueues();
            this.connector = connector;
        }

        /// <summary>
        /// 重置发送队列
        /// </summary>
        public abstract void ResetQueues();



        /// <summary>
        /// 发送数据包
        /// </summary>
        /// <param name="pkg"></param>
        public abstract void sendDataPkg(TransferredPKG pkg);


        /// <summary>
        /// 回送ack包
        /// </summary>
        /// <param name="pkgID"></param>
        public abstract void sendACKPkg(uint pkgID);

        /// <summary>
        /// 停止所有发包
        /// </summary>
        public virtual void stop()
        {
            try
            {
                //清空集合
                ResetQueues();
            }
            catch { }
        }
    }

}
