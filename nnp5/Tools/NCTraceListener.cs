﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace eslib.trace
{
    /// <summary>
    /// 
    /// </summary>
    public class ESTraceListener : TraceListener
    {               

        /// <summary>
        /// 拆分category和消息
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string[] SplitCategory(string message)
        {
            return message.Split(new string[] { ": " }, StringSplitOptions.RemoveEmptyEntries);
        }



        public override void Write(string message)
        {
            NewTraceMessageEvent?.Invoke(message);

            string[] msg = SplitCategory(message);
            if (msg.Length == 2)
            {
                NewTraceCategoryMessageEvent?.Invoke(msg[0], msg[1]);
            }
        }

        public override void WriteLine(string message)
        {
            NewTraceMessageEvent?.Invoke($"{message}\r\n");

            string[] msg = SplitCategory(message);
            if (msg.Length == 2)
            {
                NewTraceCategoryMessageEvent?.Invoke(msg[0], $"{msg[1]}\r\n");
            }
        }




        /// <summary>
        /// 新消息事件
        /// </summary>
        public event NewTraceMessageEventHandler NewTraceMessageEvent;


        /// <summary>
        /// 带category的消息事件
        /// </summary>
        public event NewTraceCategoryMessageEventHandler NewTraceCategoryMessageEvent;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    public delegate void NewTraceMessageEventHandler(string message);

    /// <summary>
    /// /
    /// </summary>
    /// <param name="category"></param>
    /// <param name="message"></param>
    public delegate void NewTraceCategoryMessageEventHandler(string category, string message);

}
