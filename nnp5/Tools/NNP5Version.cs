﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eslib.nnp5
{
    /// <summary>
    /// nnp5信息
    /// </summary>
    public static class NNP5Info
    {
        /// <summary>
        /// 版本
        /// </summary>
        public static string Version { get; } = "nnp v5.0";

        /// <summary>
        /// 
        /// </summary>
        public static string Category { get; } = "NNP5";
    }
}
