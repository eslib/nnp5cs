﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5.Tools
{
    /// <summary>
    /// 控制台控制器
    /// </summary>
    public class ConsoleContorler
    {
        /// <summary>
        /// 请取行处理，当函数返回时，程序结束
        /// </summary>
        public void ReadLine()
        {
            bool eventRes = false;

            while (!eventRes)
            {
                string line = Console.ReadLine().Trim();

                if (ReadLineEvent != null)
                {
                    eventRes = ReadLineEvent(line);
                }
                else
                {
                    eventRes = true;
                    Console.WriteLine("ReadLineEvent为null");
                }
            }
        }


        /// <summary>
        /// 行需要处理时引发，返回true退出
        /// </summary>
        public event ReadLineHandler ReadLineEvent;
    }


    /// <summary>
    /// 返回true时处理程序结束
    /// </summary>
    /// <param name="line"></param>
    /// <returns></returns>
    public delegate bool ReadLineHandler(string line);
}
