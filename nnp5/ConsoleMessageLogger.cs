﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5
{
    /// <summary>
    /// 控制台消息记录器
    /// </summary>
    public class ConsoleMessageLogger : MessageLogger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public override void Echo(MessageType type, string TAG, string message)
        {
            switch (type)
            {
                case MessageType.Log:
                    Console.WriteLine(message);
                    break;

                default:
                    Console.WriteLine($"{type} in {TAG}:{message}");
                    break;
            }
        }
    }
}
