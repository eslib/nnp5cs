﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace eslib.nnp5
{

    /// <summary>
    /// 消息记录器
    /// </summary>
    public abstract class MessageLogger
    {
        /// <summary>
        /// 获取或设置记录消息级别,默认为Warn(接收所有消息)
        /// </summary>
        public MessageLogLevel LogLevel { get; set; } = MessageLogLevel.Warn;

        /// <summary>
        /// 输出方法
        /// </summary>
        /// <param name="type"></param>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public abstract void Echo(MessageType type, string TAG, string message);


        /// <summary>
        /// 发送错误消息
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Error(string TAG, string message)
        {
            if (((int)LogLevel) >= ((int)(MessageLogLevel.Error)))
                Echo(MessageType.Error, TAG, message);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="msg"></param>
        public void Log(string msg)
        {
            Echo(MessageType.Log, "", msg);
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Log(string TAG, string message)
        {
            Echo(MessageType.Log, TAG, message);
        }

        /// <summary>
        /// 发送警告消息
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Warn(string TAG, string message)
        {
            if (((int)LogLevel) >= ((int)(MessageLogLevel.Warn)))
                Echo(MessageType.Warn, TAG, message);
        }


        /// <summary>
        /// 消息类型
        /// </summary>
        public enum MessageType
        {
            Log, Warn, Error
        }
    }



    /// <summary>
    /// 消息记录级别 
    /// </summary>
    public enum MessageLogLevel
    {
        /// <summary>
        /// 接收所有消息（包括error,warn,log）
        /// </summary>
        Warn = 2,

        /// <summary>
        /// 接收error和log，不接收warn
        /// </summary>
        Error = 1,

        /// <summary>
        /// 仅接收log
        /// </summary>
        Log = 0
    }

}
