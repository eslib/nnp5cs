﻿using MongoDB.Bson;
using System;

namespace eslib.nnp5.serializer
{
    /// <summary>
    /// 使用bson序列化
    /// </summary>
    public class BsonSerializer : ISerializer
    {
        public T Deserialize<T>(byte[] buf) where T : new()
        {
            return MongoDB.Bson.Serialization.BsonSerializer.Deserialize<T>(buf);
        }

        public object Deserialize(Type t, byte[] buf)
        {
            return MongoDB.Bson.Serialization.BsonSerializer.Deserialize(buf, t);
        }

        public byte[] Serialize(object obj)
        {
            //object不能直接bson序列化，需要提供类型
            return obj.ToBson(obj.GetType());
        }

    }
}
