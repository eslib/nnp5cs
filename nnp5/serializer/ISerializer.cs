﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace eslib.nnp5.serializer
{
    public interface ISerializer
    {
        byte[] Serialize(object obj);

        T Deserialize<T>(byte[] buf) where T : new();

        /// <summary>
        /// 使用反射反序列化
        /// </summary>
        /// <param name="t"></param>
        /// <param name="buf"></param>
        /// <returns></returns>
        object Deserialize(Type t, byte[] buf);

    }
}
