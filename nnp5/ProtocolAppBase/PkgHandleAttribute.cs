﻿using System;

namespace eslib.nnp5
{
    /// <summary>
    /// 数据包处理定义，必需只带一个参数（处理的数据包的类型）
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class PkgHandleAttribute : Attribute
    {
        public PkgHandleAttribute()
        {
        }
    }
}
