﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.DataRetrievalLib.Matchers
{
    public abstract class MatcherBase
    {
        protected DataRetrieval dataRetrieval;

        public MatcherBase()
        {
            dataRetrieval = new DataRetrieval();
            dataRetrieval.matchEvent += DataRetrieval_matchEvent;

            //添加处理器
            addHandlers();

            //激活处理器，必需从0开始
            dataRetrieval.setActiveHandler(0);
        }


        /// <summary>
        /// 在此方法中添加处理器
        /// </summary>
        protected abstract void addHandlers();

        public void reset()
        {
            dataRetrieval.reset();
        }


        public void inStream(byte ch)
        {
            dataRetrieval.inStream(ch); ;
        }

        public void inStream(byte[] buf)
        {
            for (int i = 0; i < buf.Length; i++) inStream(buf[i]);
        }




        private void DataRetrieval_matchEvent(byte[] buf)
        {
            matchEvent?.Invoke(buf);
        }


        /// <summary>
        /// 成功匹配后引发
        /// </summary>
        public event match_handle matchEvent;
    }
}
