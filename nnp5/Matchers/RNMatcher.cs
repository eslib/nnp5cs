﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.DataRetrievalLib.Matchers
{
    public class RNMatcher : MatcherBase
    {
        DRHandler handler_rn;

        protected override void addHandlers()
        {
            //添加处理器
            handler_rn = new DRHandler(dataRetrieval, DRHandlerUnfeatureType.UNFEATURE_HEAD_ENQUEUE, Encoding.ASCII.GetBytes("\r\n"));
            dataRetrieval.addHandler(handler_rn);
        }
    }
}
