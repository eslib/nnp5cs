﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.DataRetrievalLib.Matchers
{
    /// <summary>
    /// AT指令匹配器
    /// </summary>
    public class ATMatcher : MatcherBase
    {
        DRHandler handler_at;
        DRHandler handler_rn;


        protected override void addHandlers()
        {
            //添加处理器
            handler_at = new DRHandler(dataRetrieval, DRHandlerUnfeatureType.UNFEATURE_HEAD_NON_ENQUEUE, Encoding.ASCII.GetBytes("AT+"));
            dataRetrieval.addHandler(handler_at);

            handler_rn = new DRHandler(dataRetrieval, DRHandlerUnfeatureType.UNFEATURE_HEAD_ENQUEUE, Encoding.ASCII.GetBytes("\r\n"));
            dataRetrieval.addHandler(handler_rn);
        }


    }

}
