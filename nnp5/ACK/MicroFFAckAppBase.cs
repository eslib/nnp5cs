﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eslib.nnp5.ACK
{


    /// <summary>
    /// 使用MicroFF协议的带Ack应用层基类
    /// </summary>
    public abstract class MicroFFAckAppBase : MicroFFAppBase
    {
        /// <summary>
        /// ack系统
        /// </summary>
        public MicroFFAck GenericAck { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connector"></param>
        /// <param name="ackTimeoutMs"></param>
        public MicroFFAckAppBase(Connector connector, int ackTimeoutMs = 1000) : base(connector)
        {
            GenericAck = new MicroFFAck(this, ackTimeoutMs);
            GenericAck.NewPkgEvent += NewPkgEvent_AckLayer;
            GenericAck.AckEvent += AckEvent_AckLayer;
            GenericAck.AckTimeoutEvent += AckTimeoutEvent_AckLayer;
        }


        /// <summary>
        /// 异步
        /// </summary>
        /// <param name="oriBuffer"></param>
        public override void NewPKGEvent(byte[] oriBuffer)
        {
            GenericAck.RecvBufferEvt(oriBuffer);
        }


        /// <summary>
        /// 基类发送数据包方法
        /// </summary>
        /// <param name="buffer"></param>
        internal void send_buffer(byte[] buffer)
        {
            base.SendPkg(buffer);
        }


        /// <summary>
        /// Ack层 发送数据包
        /// </summary>
        /// <param name="pkg"></param>
        public override void SendPkg(IPackage pkg)
        {
            GenericAck.SendPkg(pkg.GetBuffer());
        }

        /// <summary>
        /// Ack层 发送数据包
        /// </summary>
        /// <param name="pkg"></param>
        public override void SendPkg(byte[] pkg)
        {
            GenericAck.SendPkg(pkg);
        }






        #region Ack层应用方法 

        /// <summary>
        /// Ack层 新数据包事件
        /// </summary>
        /// <param name="buffer"></param>
        public abstract void NewPkgEvent_AckLayer(byte[] buffer);

        /// <summary>
        /// Ack层 收到Ack事件
        /// </summary>
        public abstract void AckEvent_AckLayer();

        /// <summary>
        /// Ack层 Ack超时事件
        /// </summary>
        public abstract void AckTimeoutEvent_AckLayer();

        #endregion
    }

}
