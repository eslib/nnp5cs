﻿namespace eslib.nnp5.ACK
{
    /// <summary>
    /// 使用MicroFF协议的Ack层
    /// </summary>
    public class MicroFFAck : GenericACK
    {
        MicroFFAckAppBase app;

        /// <summary>
        /// 构造Ack层
        /// </summary>
        /// <param name="app"></param>
        /// <param name="ackTimeoutMs"></param>
        public MicroFFAck(MicroFFAckAppBase app, int ackTimeoutMs = 1000) : base(ackTimeoutMs)
        {
            this.app = app;

        }

        /// <summary>
        /// 实际数据的发送实现
        /// </summary>
        /// <param name="buffer"></param>
        protected override void SendBuffer(byte[] buffer)
        {
            app.send_buffer(buffer);
        }



        internal void RecvBufferEvt(byte[] buffer)
        {
            base.RecvBufferEvent(buffer);
        }
    }

}
