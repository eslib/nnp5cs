﻿using System;
using System.Collections.Generic;
using System.Text;
using eslib.nnp5.layers;

namespace eslib.nnp5
{
    /// <summary>
    /// 服务端模块定义
    /// 派生类应只实现监听器和观察者的创建，不应产生额外资源占用
    /// 
    /// 对应配置文件
    /// {
    ///  "mod": "服务名称",
    ///  "bind": "端口",
    ///  "maxClient":512    //最大连接数
    ///  }
    /// </summary>
    public abstract class ServerModBase
    {

        /// <summary>
        /// 模块名称
        /// </summary>
        public abstract string ModName { get; }


        /// <summary>
        /// 创建新监听器
        /// </summary>
        public abstract ServerListener NewListener();


        /// <summary>
        /// 创建新监听器事件观察者
        /// </summary>
        public abstract ListenerEventObserver NewObserver();


        /// <summary>
        /// [服务端调用] 
        /// 创建新监听器
        /// </summary>
        /// <returns></returns>
        public ServerListener CreateListener()
        {
            ServerListener lsn = NewListener();

            ListenerEventObserver obsv = NewObserver();
            obsv.ModBase = this;

            lsn.NewConnectionEvent += obsv.NewConnection;
            return lsn;
        }



        #region 消息记录

        /// <summary>
        /// 消息记录器(由服务器设置)
        /// </summary>
        public MessageLogger Logger { get; set; } = null;


        /// <summary>
        /// 错误记录
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Error(string TAG, string message)
        {
            if (Logger == null) return;
            Logger.Error(TAG, message);
        }


        /// <summary>
        /// 记录
        /// </summary>
        /// <param name="msg"></param>
        public void Log(string msg)
        {
            if (Logger == null) return;
            Logger.Log(msg);
        }


        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Warn(string TAG, string message)
        {
            if (Logger == null) return;
            Logger.Warn(TAG, message);
        }

        #endregion
    }




    /// <summary>
    /// 监听器事件观察者抽象
    /// </summary>
    public abstract class ListenerEventObserver
    {
        /// <summary>
        /// 由ServerModBase自动设置
        /// </summary>
        public ServerModBase ModBase { get; internal set; }



        /// <summary>
        /// 新连接传入事件
        /// </summary>
        /// <param name="connector"></param>
        public abstract void NewConnection(Connector connector);





        #region 消息记录

        /// <summary>
        /// 错误记录
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Error(string TAG, string message)
        {
            ModBase.Error(TAG, message);
        }


        /// <summary>
        /// 记录
        /// </summary>
        /// <param name="msg"></param>
        public void Log(string msg)
        {
            ModBase.Log(msg);
        }


        /// <summary>
        /// 警告
        /// </summary>
        /// <param name="TAG"></param>
        /// <param name="message"></param>
        public void Warn(string TAG, string message)
        {
            ModBase.Warn(TAG, message);
        }

        #endregion
    }
}
