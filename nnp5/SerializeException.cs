﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eslib.CoreSerialization
{
    public class SerializeException : Exception
    {
        public SerializeException(string message) : base(message)
        {
        }
    }
}
