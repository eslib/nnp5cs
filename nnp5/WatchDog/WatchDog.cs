﻿using System;

namespace eslib.nnp5.Tools
{
    /// <summary>
    /// 看门狗,超时引发事件,只会触发一次
    /// 可用于服务端的AppUserBase作网络活动监听（超时断开连接）使用
    /// </summary>
    public class WatchDog : IDisposable
    {
        Worker worker;

        /// <summary>
        /// 喂狗标志
        /// </summary>
        bool FreeDogCalled
        {
            get
            {
                lock (this)
                {
                    return freeDogCalled;
                }
            }
            set
            {
                lock (this)
                {
                    freeDogCalled = value;
                }
            }
        }
        private bool freeDogCalled = true;
        private bool disposedValue;

        public WatchDog()
        {
        }


        /// <summary>
        /// 启动看门狗
        /// </summary>
        /// <param name="timeoutMs">超时时间（毫秒)</param>
        public void Enable(int timeoutMs)
        {
            worker = new Worker(timeoutMs);
            _ = worker.RunCycle(workerEvt);
        }


        //工作项
        void workerEvt()
        {
            if (FreeDogCalled)      //初次运行和喂狗后必定为true
            {
                //执行后置为false
                FreeDogCalled = false;
            }
            else        //标志为false,说明中间没有喂狗
            {
                worker.Cancel();        //结束工作线程
                WatchDogTimeoutEvent?.Invoke();
            }
        }


        /// <summary>
        /// 喂狗
        /// </summary>
        public void FreeDog()
        {
            FreeDogCalled = true;
        }


        /// <summary>
        /// 看门狗超时事件,只会触发一次
        /// </summary>
        public event WatchDogTimeoutEventDelegate WatchDogTimeoutEvent;


        #region 释放模式

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 释放托管状态(托管对象)
                    try
                    {
                        worker.Cancel();//结束工作线程
                    }
                    catch { }
                }

                // TODO: 释放未托管的资源(未托管的对象)并替代终结器
                // TODO: 将大型字段设置为 null
                disposedValue = true;
            }
        }

        // // TODO: 仅当“Dispose(bool disposing)”拥有用于释放未托管资源的代码时才替代终结器
        // ~WatchDog()
        // {
        //     // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // 不要更改此代码。请将清理代码放入“Dispose(bool disposing)”方法中
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }

    /// <summary>
    /// 看门狗超时事件委托
    /// </summary>
    public delegate void WatchDogTimeoutEventDelegate();
}
