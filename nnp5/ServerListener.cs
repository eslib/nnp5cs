﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eslib.nnp5
{
    /// <summary>
    /// 服务端抽象
    /// </summary>
    public abstract class ServerListener
    {
        /// <summary>
        /// 绑定本地特征(如地址/端口)
        /// </summary>
        /// <param name="property"></param>
        public abstract void bind(object property);


        /// <summary>
        /// 开始监听
        /// </summary>
        public abstract void listenStart();

        /// <summary>
        /// 结束监听
        /// </summary>
        public abstract void listenStop();


        /// <summary>
        /// 关闭服务器
        /// </summary>
        public abstract void close();


        /// <summary>
        /// 新连接进入时引发
        /// </summary>
        public event NewConnectionEventHandler NewConnectionEvent;

        /// <summary>
        /// 引发新连接事件
        /// </summary>
        /// <param name="connector"></param>
        protected void InvokeNewConnectionEvent(Connector connector)
        {
            new Task(() =>
            {
                NewConnectionEvent?.Invoke(connector);
            }).Start();
        }
    }


    /// <summary>
    /// 新连接事件委托
    /// </summary>
    /// <param name="connector"></param>
    public delegate void NewConnectionEventHandler(Connector connector);
}
