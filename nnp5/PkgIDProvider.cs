﻿using System;

namespace eslib.nnp5.layers
{
    internal class PkgIDProvider
    {
        //线程同步
        private volatile uint id = 0;

        /// <summary>
        /// 分配下一个包id
        /// </summary>
        /// <returns></returns>
        internal uint next()
        {
            return id++;
        }
    }
}