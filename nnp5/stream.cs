﻿using System;
using System.Collections.Generic;

namespace eslib.DataRetrievalLib
{
    class stream
    {
        public stream()
        {
            io = new List<byte>();
        }


        //写入流
        public void write(byte ch)
        {
            io.Add(ch);
        }

        //流长度
        public int len()
        {
            return io.Count;
        }

        //清空流,使用完后需要手工执行
        public void clear()
        {
            io.Clear();
        }

        //获取流数据
        public byte[] get()
        {
            return io.ToArray();
        }



        private List<byte> io;
    }
}
