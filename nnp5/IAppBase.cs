﻿namespace eslib.nnp5
{
    /// <summary>
    /// 通讯App接口
    /// </summary>
    public interface IAppBase
    {
        object BindingObject { get; set; }
        bool Connected { get; set; }

        void Close();
        void DisconnectEvent(Connector connector, string message);
        void NewPKGEvent(byte[] oriBuffer);
        void Reset();
        void SendPkg(IPackage pkg);
    }
}