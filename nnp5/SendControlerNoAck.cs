﻿namespace eslib.nnp5.layers
{
    /// <summary>
    /// 发送控制器（直接发送，不等待ack版本)
    /// </summary>
    class SendControlerNoAck : SendControlerBase
    {
        public SendControlerNoAck(Connector connector) : base(connector)
        {
        }

        public override void ResetQueues()
        {
        }

        public override void sendACKPkg(uint pkgID)
        {
            ACKPkg ack = ACKPkg.CreateSuccessACK(pkgID);
            lock (this)
            {
                connector.send(ack.makePKG());
            }
        }

        public override void sendDataPkg(TransferredPKG pkg)
        {
            lock (this)
            {
                connector.send(pkg.makePKG());
            }
        }
    }

}
