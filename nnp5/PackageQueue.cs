﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace eslib.nnp5
{
    /// <summary>
    /// 数据包队列(线程同步的）
    /// </summary>
    public class PackageQueue
    {
        ConcurrentQueue<IPackage> pkgQueue = new ConcurrentQueue<IPackage>();




        /// <summary>
        /// 队列中是否有数据包
        /// </summary>
        /// <returns></returns>
        public bool HasPkg()
        {
            return pkgQueue.Count > 0;
        }


        /// <summary>
        /// 入列
        /// </summary>
        /// <param name="pkg"></param>
        public void Enqueue(IPackage pkg)
        {
            pkgQueue.Enqueue(pkg);
        }




        /// <summary>
        /// 出列
        /// </summary>
        /// <returns></returns>
        public (bool success, IPackage pkg) Dequeue()
        {
            IPackage p = null;
            bool suc = pkgQueue.TryDequeue(out p);
            return (suc, p);
        }
    }
}
